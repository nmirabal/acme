<!--El controlador debe recibir y verificar los datos enviados desde las view.-->
<?php
/*
* PRODUCTS CONTROLLER
*
*/
// Create or access a Session
 session_start();
// Get the database connection file
 require_once '../library/connections.php';
 // Get the acme model for use as needed
 require_once '../model/acme-model.php';
//Get the accounts model
require_once '../model/products-model.php';
//Get the functions page
require_once '../library/functions.php';
//Get the function from uploads-model:
require_once '../model/uploads-model.php';
//Get the function from reviews-model:
require_once '../model/reviews-model.php';

// Get the array of categories
$categories = getCategories();
//var_dump($categories);
//exit; 

$action = filter_input(INPUT_POST, 'action');
 if ($action == NULL){
  $action = filter_input(INPUT_GET, 'action');
 }


// Build a navigation bar using the $categories array
 $navList = dynamicNavigation($categories); 
 
//en este caso el switch sirve para crear la coneccion con new-cat y new-prod.php para que se abran todos en la opcion products/index.php
switch ($action){
case 'catForm':
 include '../view/new-cat.php';
 break;

    case 'prodForm':
        include '../view/new-prod.php';
    break;
        
case 'newProd':
  //echo '<pre>' . print_r($_POST, true) . '</pre>'; AVERIGUAR FUNCIONALIDAD DE ESTA PARTE.
  $categoryId = filter_input(INPUT_POST, 'categoryId', FILTER_SANITIZE_NUMBER_INT);
  $invName = filter_input(INPUT_POST, 'invName', FILTER_SANITIZE_STRING);
  $invDescription = filter_input(INPUT_POST, 'invDescription', FILTER_SANITIZE_STRING);
  $invImage = filter_input(INPUT_POST, 'invImage', FILTER_SANITIZE_STRING);
  $invThumbnail = filter_input(INPUT_POST, 'invThumbnail', FILTER_SANITIZE_STRING);
  $invPrice = filter_input(INPUT_POST, 'invPrice', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
  $invStock = filter_input(INPUT_POST, 'invStock', FILTER_SANITIZE_NUMBER_INT);
  $invSize = filter_input(INPUT_POST, 'invSize', FILTER_SANITIZE_NUMBER_INT);
  $invWeight = filter_input(INPUT_POST, 'invWeight', FILTER_SANITIZE_NUMBER_INT);
  $invLocation = filter_input(INPUT_POST, 'invLocation', FILTER_SANITIZE_STRING);
  $invVendor = filter_input(INPUT_POST, 'invVendor', FILTER_SANITIZE_STRING);
  $invStyle = filter_input(INPUT_POST, 'invStyle', FILTER_SANITIZE_STRING);
  //Check for missing data-ESTO SIRVE PARA QUE EL USUARIO COMPLETE TODOS LOS CAMPOS REQUERIDOS!
  if (empty($invName) || empty($invDescription) || empty($invImage) ||
          empty($invThumbnail) || empty($invPrice) || empty($invStock) || empty($invSize) ||
          empty($invWeight) || empty($invLocation) || empty($categoryId) || empty($invVendor) ||
          empty($invStyle)) {
      //El mansaje en caso de que NO se complete todo el formulario
   $message = '<p>Please provide information for all empty form fields.</p>';
   include $_SERVER['DOCUMENT_ROOT'].'/acme/view/new-prod.php';
  
  }
  // Send the data to the model
  $newProductOutcome = newProd($categoryId, $invName, $invDescription, $invImage, $invThumbnail, $invPrice, $invStock, $invSize, $invWeight, $invLocation, $invVendor, $invStyle);
  // Check and report the result
  if ($newProductOutcome === 1) {
   $message = "<p class='notice'>Thank you. The product has been added to the inventory.</p>";
   include $_SERVER['DOCUMENT_ROOT'].'/acme/view/new-prod.php';
   
  } else {
   $message = "<p>Sorry, new product was not created. Please try again.</p>";
   include $_SERVER['DOCUMENT_ROOT'].'/acme/view/new-prod.php';
   
  }
  break;
  
 // new category option in the prod-mgmt.php
  case 'newCat':
  // Filter and store the data
  $categoryName = filter_input(INPUT_POST, 'categoryName', FILTER_SANITIZE_STRING);
  //Check for missing data
  if (empty($categoryName)) {
   $message = '<p>Please provide information for all empty form fields.</p>';
   include $_SERVER['DOCUMENT_ROOT'].'/acme/view/new-cat.php';
   
  }
  //Send data to the model
  $newCategoryOutcome = newCat($categoryName);
  //Check and report the result
  if ($newCategoryOutcome === 1) {
   header("location: /acme/products/index.php");
   
  } else {
   $message = '<p class="form-error">Sorry, new category was not created. Please try again.</p>';
   include $_SERVER['DOCUMENT_ROOT'].'/acme/view/new-cat.php';
   
  }
  break;     
  


  //este case es para la opcion MODIFICAR de la lista de productos
case 'mod':
$invId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
/*
A continuación, enviaremos la variable $ prodId a una función 
que obtendrá la información para ese único producto:
*/
$prodInfo = getProductInfo($invId);
//seguido,comprobaremos si $ prodInfo tiene datos. Si no es así, vamos a configurar un mensaje de error:
if(count($prodInfo)<1){
  $message = 'Sorry, no product information could be found.';
 }
 //Finally we will call a view where the data can be displayed so that the changes can be made to the data:
 include '../view/prod-update.php';
 exit;
 break;

 case 'updateProd':
 //filtrar y recoger los datos
    $categoryId = filter_input(INPUT_POST, 'categoryId', FILTER_SANITIZE_NUMBER_INT);
    $invName = filter_input(INPUT_POST, 'invName', FILTER_SANITIZE_STRING);
    $invDescription = filter_input(INPUT_POST, 'invDescription', FILTER_SANITIZE_STRING);
    $invImg = filter_input(INPUT_POST, 'invImage', FILTER_SANITIZE_STRING);
    $invThumb = filter_input(INPUT_POST, 'invThumbnail', FILTER_SANITIZE_STRING);
    $invPrice = filter_input(INPUT_POST, 'invPrice', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $invStock = filter_input(INPUT_POST, 'invStock', FILTER_SANITIZE_NUMBER_INT);
    $invSize = filter_input(INPUT_POST, 'invSize', FILTER_SANITIZE_NUMBER_INT);
    $invWeight = filter_input(INPUT_POST, 'invWeight', FILTER_SANITIZE_NUMBER_INT);
    $invLocation = filter_input(INPUT_POST, 'invLocation', FILTER_SANITIZE_STRING);
    $invVendor = filter_input(INPUT_POST, 'invVendor', FILTER_SANITIZE_STRING);
    $invStyle = filter_input(INPUT_POST, 'invStyle', FILTER_SANITIZE_STRING);
    $invId = filter_input(INPUT_POST, 'invId', FILTER_SANITIZE_NUMBER_INT);

//Chequeo por informacion faltante o mala informacion
 if (empty($categoryId) || empty($invName) || empty($invDescription) || empty($invImg) ||
 empty($invThumb) || empty($invPrice) || empty($invStock) || empty($invSize) || empty($invWeight) || empty($invLocation) || empty($invVendor) || empty($invStyle)) {
  echo 'here';
  exit;
  //si hay errores encontrados arrojará un mesaje como el siguiente:
  $message = "<p class='notice'> Please complete all information for the updated item! Double check the category of the item.</p>";
   include '../view/prod-update.php';
 exit;
 //si no hay errores se procesa la actualizacion
}  
$updateResult = updateProduct($categoryId, $invName, $invDescription, $invImg, $invThumb, $invPrice, $invStock, $invSize, $invWeight, $invLocation, $invVendor, $invStyle, $invId);
//reportar el resultado al navegador
if ($updateResult) {
  $message = "<p class='notice'>  Congratulations, $invName was successfully updated.</p>";
  $_SESSION['message'] = $message;
  header('location: /acme/products/');
  exit;
 }
  else {
  $message = "<p class='notice' >Error. $invName the product was not updated.</p>";
 include '../view/prod-update.php';
 exit;
}
 break;


 //enhancemet-8:
 case 'category':
 // escriba el código para filtrar, sanear y almacenar el segundo valor que se envía a través de la URL
 //(recuerde que las URL se envían automáticamente usando "GET"):
 $categoryName = filter_input(INPUT_GET, 'categoryName', FILTER_SANITIZE_STRING);
 $products = getProductsByCategory($categoryName);
 /*se construirá una estructura de control if - else para ver si los productos realmente fueron devueltos o no.
 *Si "NO" se generará un mensaje de error
 *Si la respuesta es "Sí", la matriz de productos se enviará a la función personalizada para compilar el HTML 
 *en torno a los productos y devolvérnoslo para su visualización:
 */ 
 if(!count($products)){
  $message = "<p class='notice'>Sorry, no $categoryName products could be found.</p>";
 } else {
  $prodDisplay = buildProductsDisplay($products);
  //estas dos lineas siguientes sirven para hacer un test si funciona, (luego deben ser removidas):
}
//Finalmente, se llamará a una vista para mostrar el mensaje o los productos que pertenecen a la categoría en la que se hizo clic en la barra de navegación:
 include '../view/category.php';
 break;

 case 'prod-details':
 $invId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
 $productInfo = getProductInfo($invId);
 //$productThumbnails = getProductThumbnails($invId);
 
 if(!count($productInfo) == 1){
   $message = "<p>Sorry, the product info could not be found.</p>";
 } else {
  $reviews = getReviewsByInvId($invId);
  
  $_SESSION['reviews'] = $reviews;

  $reviewDisplay = buildReviewDisplay($reviews);
  $prodDisplay = buildProductInfoDisplay($productInfo);
  //$thumbnailDisplay = buildThumbnailDisplay($productThumbnails);
 }
  include $_SERVER['DOCUMENT_ROOT'].'/acme/view/prod-details.php';
  break;


    //is responsible for delivering the "product management" view.
    default:
    //el codigo a continuacion es para crear una TABLA QUE MUESTRA LOS PRODUCTOS QUE ESTAN EL LA BASE DE DATOS
    $products = getProductBasics();
    if(count($products) > 0){
     $prodList = '<table>';
     $prodList .= '<thead>';
     $prodList .= '<tr><th>Product Name</th><td>&nbsp;</td><td>&nbsp;</td></tr>';
     $prodList .= '</thead>';
     $prodList .= '<tbody>';
     foreach ($products as $product) {
       //Cell one has the product name displayed from the array row.
      $prodList .= "<tr><td>$product[invName]</td>";
      //This will allow us to indicate we want to modify a product and identify which product to modify.
      $prodList .= "<td><a href='/acme/products?action=mod&id=$product[invId]' title='Click to modify'>Modify</a></td>";
      //his will allow us to indicate we want to delete a product and identify which product to delete.
      $prodList .= "<td><a href='/acme/products?action=del&id=$product[invId]' title='Click to delete'>Delete</a></td>";
      //la siguiente linea será para mostrar las caracteristicas del producto
      $prodList .= "<td><a href='/acme/products?action=fea&id=$product[invId]' title='Click to see the features'>Feature</a></td></tr>";
     }
     //When the loop is finished the </tbody> and </table> elements are closed.
      $prodList .= '</tbody></table>';
     } else {
       //Note el uso mixto de comillas. Cuando se crea una cadena simple se usan comillas simples. 
       //Cuando se construye una cadena que necesita mostrar un valor de la matriz, se usan comillas dobles
      $message = '<p class="notice">Sorry, no products were returned.</p>';
   }
        include '../view/prod-mgmt.php';
        break;


//este es el CASE para ELIMINAR UN PRODUCTO
      case 'del':
      $invId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
      $prodInfo = getProductInfo($invId);
      if (count($prodInfo) < 1) {
      $message = 'Sorry, no product information could be found.';
      }
      include '../view/prod-delete.php';
      exit;
      break;
      


      case 'deleteProd':
      $invName = filter_input(INPUT_POST, 'invName', FILTER_SANITIZE_STRING);
      $invId = filter_input(INPUT_POST, 'invId', FILTER_SANITIZE_NUMBER_INT);
     
      $deleteResult = deleteProduct($invId);
      if ($deleteResult) {
       $message = "<p class='notice'>Congratulations, $invName was successfully deleted.</p>";
       $_SESSION['message'] = $message;
       header('location: /acme/products/');
       exit;
      } else {
       $message = "<p class='notice'>Error: $invName was not deleted.</p>";
       $_SESSION['message'] = $message;
       header('location: /acme/products/');
       exit;
      }
      break;
    

//ESTE CURLYBRACE ES DEL CIERRE DEL switch
}
?>