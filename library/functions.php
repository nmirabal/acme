<?php 
//In the functions.php file we have created two custom functions: 1) to validate an email address - checkEmail() - and, 2) to check a password that it meets specific requirements - checkPassword().
function checkEmail($clientEmail){
    //The end result will be returned. It will be one of two values: 1) The actual email address will be returned if it is judged to be "valid", or 2) NULL - indicating the email does not appear to be a valid address.
    $valEmail = filter_var($clientEmail, FILTER_VALIDATE_EMAIL);
 return $valEmail;
}


// Check the password for a minimum of 8 characters,
 // at least one 1 capital letter, at least 1 number and
 // at least 1 special character
function checkPassword($clientPassword){
 $pattern = '/^(?=.*[[:digit:]])(?=.*[[:punct:]])(?=.*[A-Z])(?=.*[a-z])([^\s]){8,}$/';
 return preg_match($pattern, $clientPassword);
}

//dynamic navigation
function dynamicNavigation($categories){
    //Build a navigation bar using the $categories array
    $navList = '<nav><ul>';
    $navList .= "<li><a href= '/acme/' title='View the Acme home page'> Home</a>
    </li>";
    foreach ($categories as $category) {
//A)Los ? en la ruta representa un indicador para el servidor que un par de nombre-valor se envía como un parámetro a través de la URL.
//A)En nuestro caso, el nombre es "acción" y el valor es "categoría".
//B)El & en la ruta representa que se está enviando otro par de nombre-valor 
//B)(sí, estos pueden repetirse para tantos pares de nombre-valor como quiera enviar).In our code, the second "name" is categoryName and its value is "$category[categoryName]" meaning the actual name of that category (e.g. Trap, Cannon, etc...).
    $navList .="<li><a href='/acme/products/?action=category&categoryName=" .urlencode($category['categoryName']). "' title='View our $category[categoryName] product line'>$category[categoryName]</a></li>";
  }
  $navList .='</ul></nav>';
  return $navList;
}
    


//La nueva función construirá una exhibición de productos dentro de una lista desordenada.
function buildProductsDisplay($products){
  $pd = '<ul id="prod-display">';
  foreach ($products as $product) {
    $pd .= '<li>'."\n";
    $pd .= "<a href='/acme/products?action=prod-details&id=$product[invId]' title='Click to view this product' style='text-decoration:none;'>"."\n";
    $pd .= '<div>'."\n";
    $pd .= "<img src='$product[invThumbnail]' alt='Image of $product[invName] on Acme.com'>"."\n";
    $pd .= '</div><!--  -->'."\n";
    $pd .= '<hr>'."\n";
    $pd .= "<h2 style='font-size:22px;'>$product[invName]</h2>"."\n";
    $pd .= "<span>$$product[invPrice]</span>"."\n";
    $pd .= '</a>'."\n";
    $pd .= '</li>'."\n";
  }
  $pd .= '</ul>';
  return $pd;
 }

 function buildProductInfoDisplay($productInfo){
  $pd = "<h1 style='text-align: center;'>$productInfo[invName]</h1>"."\n";
  $pd .= '<div class="product-display">';
  
  $pd .= '<div class="image-display">';
    $pd .= "<img src='$productInfo[invImage]' alt='An image showing the ACME $productInfo[invName]'>"."\n";
  $pd .= '</div>';
  
  $pd .= '<div>';
    $pd .= "<p style='color:#373BB4; font-weight: bold;'>$$productInfo[invPrice]</p>"."\n";
    $pd .= "<p>"."\n";
      if ($productInfo['invStock'] < 10){
        $pd .= "<span>Only $productInfo[invStock] left. Order soon!</span></p>"."\n";
      } else {
        $pd .= "<span>In stock!</span></p>"."\n";
      }
    $pd .= "<p>$productInfo[invDescription]</p>"."\n";
    $pd .= "<p>Size: $productInfo[invSize]</p>"."\n";
    $pd .= "<p>Weight: $productInfo[invWeight] lb</p>"."\n";
    $pd .= "<p>Style: $productInfo[invStyle]</p>"."\n";
  $pd .= '</div>';
  
  $pd .= '</div>'; //product display div ends here 
  
  return $pd;
}


/* * ********************************
*  Functions for working with images
* ********************************* */

// Adds "-tn" designation to file name
function makeThumbnailName($image) {
  $i = strrpos($image, '.');
//La función substr () de PHP se usa para separar la cadena como si fuera una matriz. 
//Todo a la izquierda del período (ahora denominado $ i es el elemento cero "0" en la matriz. 
//Es el $ nombre_imagen (por ejemplo, yunque).
  $image_name = substr($image, 0, $i);
  $ext = substr($image, $i);
  //el nombre del archivo se concatena con la cadena "-tn" y la extensión se concatena hasta el final (por ejemplo, anvil-tn.jpg).
  $image = $image_name . '-tn' . $ext;
  return $image;
 }


 // Build images display for image management view
function buildImageDisplay($imageArray) {
  $id = '<ul id="image-display">';
  foreach ($imageArray as $image) {
   $id .= '<li>';
   $id .= "<img src='$image[imgPath]' title='$image[invName] image on Acme.com' alt='$image[invName] image on Acme.com'>";
   $id .= "<p><a href='/acme/uploads?action=delete&imgId=$image[imgId]&filename=$image[imgName]' title='Delete the image'>Delete $image[imgName]</a></p>";
   $id .= '</li>';
  }
  $id .= '</ul>';
  return $id;
 }


 // Build the products select list
function buildProductsSelect($products) {
  $prodList = '<select name="invId" id="invId">';
  $prodList .= "<option>Choose a Product</option>";
  foreach ($products as $product) {
   $prodList .= "<option value='$product[invId]'>$product[invName]</option>";
  }
  $prodList .= '</select>';
  return $prodList;
 }



 // Handles the file upload process and returns the path
// The file path is stored into the database
function uploadFile($name) {
  // Gets the paths, full and local directory
  global $image_dir, $image_dir_path;
  if (isset($_FILES[$name])) {
   // Gets the actual file name
   $filename = $_FILES[$name]['name'];
   if (empty($filename)) {
    return;
   }
  // Get the file from the temp folder on the server
  $source = $_FILES[$name]['tmp_name'];
  // Sets the new path - images folder in this directory
  $target = $image_dir_path . '/' . $filename;
  // Moves the file to the target folder
  move_uploaded_file($source, $target);
  // Send file for further processing
  processImage($image_dir_path, $filename);
  // Sets the path for the image for Database storage
  $filepath = $image_dir . '/' . $filename;
  // Returns the path where the file is stored
  return $filepath;
  }
 }



 // Processes images by getting paths and 
// creating smaller versions of the image
function processImage($dir, $filename) {
  // Set up the variables
  $dir = $dir . '/';
 
  // Set up the image path
  $image_path = $dir . $filename;
 
  // Set up the thumbnail image path
  $image_path_tn = $dir.makeThumbnailName($filename);
 
  // Create a thumbnail image that's a maximum of 200 pixels square
  resizeImage($image_path, $image_path_tn, 200, 200);
 
  // Resize original to a maximum of 500 pixels square
  resizeImage($image_path, $image_path, 500, 500);
 }



 // Checks and Resizes image
function resizeImage($old_image_path, $new_image_path, $max_width, $max_height) {
     
  // Get image type
  $image_info = getimagesize($old_image_path);
  $image_type = $image_info[2];
 
  // Set up the function names
  switch ($image_type) {
  case IMAGETYPE_JPEG:
   $image_from_file = 'imagecreatefromjpeg';
   $image_to_file = 'imagejpeg';
  break;
  case IMAGETYPE_GIF:
   $image_from_file = 'imagecreatefromgif';
   $image_to_file = 'imagegif';
  break;
  case IMAGETYPE_PNG:
   $image_from_file = 'imagecreatefrompng';
   $image_to_file = 'imagepng';
  break;
  default:
   return;
 } // ends the resizeImage function
 
  // Get the old image and its height and width
  $old_image = $image_from_file($old_image_path);
  $old_width = imagesx($old_image);
  $old_height = imagesy($old_image);
 
  // Calculate height and width ratios
  $width_ratio = $old_width / $max_width;
  $height_ratio = $old_height / $max_height;
 
  // If image is larger than specified ratio, create the new image
  if ($width_ratio > 1 || $height_ratio > 1) {
 
   // Calculate height and width for the new image
   $ratio = max($width_ratio, $height_ratio);
   $new_height = round($old_height / $ratio);
   $new_width = round($old_width / $ratio);
 
   // Create the new image
   $new_image = imagecreatetruecolor($new_width, $new_height);
 
   // Set transparency according to image type
   if ($image_type == IMAGETYPE_GIF) {
    $alpha = imagecolorallocatealpha($new_image, 0, 0, 0, 127);
    imagecolortransparent($new_image, $alpha);
   }
 
   if ($image_type == IMAGETYPE_PNG || $image_type == IMAGETYPE_GIF) {
    imagealphablending($new_image, false);
    imagesavealpha($new_image, true);
   }
 
   // Copy old image to new image - this resizes the image
   $new_x = 0;
   $new_y = 0;
   $old_x = 0;
   $old_y = 0;
   imagecopyresampled($new_image, $old_image, $new_x, $new_y, $old_x, $old_y, $new_width, $new_height, $old_width, $old_height);
 
   // Write the new image to a new file
   $image_to_file($new_image, $new_image_path);
   // Free any memory associated with the new image
   imagedestroy($new_image);
   } else {
   // Write the old image to a new file
   $image_to_file($old_image, $new_image_path);
   }
   // Free any memory associated with the old image
   imagedestroy($old_image);
 } // ends the if - else began on line 36


function buildThumbnailDisplay($productThumbnails){
$pd = "<h1 style='text-align: center;'>Product Thumbnails</h1>"."\n";
$pd .= '<div class="product-display">';
// .= significa que estamos añadiendo codigo en la variable NO ESTAMOS REEMPLAZANDO la variable.
$pd .= '<div>';
  foreach ($productThumbnails as $singleImage){
    $pd .= "<img src= '$singleImage[imgPath]' alt='thumbnail of $singleImage[imgName]'>"; 
}
    $pd .= '</div>';
    $pd .= '</div>'; //product display div ends here 
    return $pd;
}



?>