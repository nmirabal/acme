<?php

function insertReview($clientId, $invId, $reviewText){
    $db = acmeConnect();
    $sql = 'insert into reviews (invId, clientId, reviewText) 
            values (:invId, :clientId, :reviewText)';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':invId', $invId, pdo::PARAM_INT);
    $stmt->bindValue(':clientId', $clientId, pdo::PARAM_INT);
    $stmt->bindValue(':reviewText', $reviewText, pdo::PARAM_STR);
    $stmt->execute();
    $rowsChanged = $stmt->rowCount();
    $stmt->closeCursor();
    return $rowsChanged;
}

function getReviewsByInvId($invId){
    $db = acmeConnect();
    $sql = 'SELECT * FROM reviews WHERE invId = :invId ORDER BY reviewDate desc';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':invId', $invId, pdo::PARAM_INT);
    $stmt->execute();
    $reviewInfo = $stmt->fetchAll(pdo::FETCH_ASSOC);
    $stmt->closeCursor();
    //echo $stmt->debugDumpParams();
   // exit;
    return $reviewInfo;
}

function getReviewByClientId($clientId){
    $db = acmeConnect();
    $sql = 'SELECT * FROM reviews WHERE clientId = :clientId';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':clientId', $clientId, pdo::PARAM_INT);
    $stmt->execute();
    $prodInfo = $stmt->fetch(pdo::FETCH_ASSOC);
    $stmt->closeCursor();
    return $prodInfo;
}

function getSpecificReview($reviewId){
    $db = acmeConnect();
    $sql = 'SELECT * FROM reviews WHERE reviewId = :reviewId';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':reviewId', $$reviewId, pdo::PARAM_INT);
    $stmt->execute();
    $reviewInfo = $stmt->fetch(pdo::FETCH_ASSOC);
    $stmt->closeCursor();
    vardump($reviewInfo);
    exit;
    return $reviewInfo;
}

function updateReview($reviewId, $reviewText){
    $db = acmeConnect();
    $sql = 'UPDATE reviews SET reviewText = :reviewText WHERE reviewId = :reviewId';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':reviewId', $reviewId, pdo::PARAM_INT);
    $stmt->bindValue(':reviewText', $reviewText, pdo::PARAM_STR);
    $stmt->execute(); 
    $rowsChanged = $stmt->rowCount();
    $stmt->closeCursor(); 
    return $rowsChanged;
}

function deleteReview($reviewId){
    $db = acmeConnect();
    $sql = 'DELETE FROM reviews WHERE reviewId = :reviewId';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':reviewId', $reviewId, PDO::PARAM_INT);
    //  echo $stmt->debugDumpParams();
    // exit;
    $stmt->execute();
    $rowsChanged = $stmt->rowCount();
    $stmt->closeCursor();
    return $rowsChanged;
}

function getClientIdbyReviewId($reviewId){
    $db = acmeConnect();
    $sql = 'SELECT clientId FROM reviews WHERE reviewId = :reviewId';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':reviewId', $reviewId, PDO::PARAM_INT);
    //  echo $stmt->debugDumpParams();
    // exit;
    $stmt->execute();
    $clientId = $stmt->fetch();
    $stmt->closeCursor();
    return $clientId;
}

function getClientByClientId($clientId){
    $db = acmeConnect();
    $sql = 'SELECT clientFirstname, clientLastname FROM clients WHERE clientId = :clientId';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':clientId', $clientId, PDO::PARAM_INT);
    $stmt->execute();
    $clientName = $stmt->fetch();
    $stmt->closeCursor();
    return $clientName;
}

function buildReviewDisplay($reviewList){
        
        if (count($reviewList) > 0) {
       
        $reviewDisplay = '<div id="reviewFormDiv" style="margin-bottom: 100px;">
                                <form action="index.php" method="post" id="reviewForm" name="reviewForm"><br>';
                foreach ($reviewList as $review) {
                    $clientId = getClientIdbyReviewId($review['reviewId']);

                    $clientName = getClientByClientId($clientId['clientId']);

                    $firstInitial = substr($clientName['clientFirstname'], 0, 1);
                    $reviewName =  $firstInitial . $clientName['clientLastname'];

                    $reviewDisplay .= '<label class="reviewLabel">Member:</label>
                                    <br><input class="reviewInput" name="clientId" 
                                        value="' .$firstInitial.'  '.$clientName['clientLastname'] .'">';
                    $reviewDisplay .= '<textarea class="reviewText" readonly 
                                        form="reviewForm">'.$review['reviewText'].'</textarea><br><br>';
                    $reviewDisplay .= '<input type="hidden" name="invId" value="'.$review['invId'].'">';
                    $reviewDisplay .= '<input type="hidden" name="clientId" value="'.$review['clientId'].'">';

                    if(isset($_SESSION['clientData']['clientId'])){
                        if ($_SESSION['clientData']['clientId'] == $clientId['clientId']){
                            $reviewDisplay .='<div class="reviewLinksContainer">'.
                            '<ul class="reviewLinks">
                            <li><a href="/acme/reviews/index.php?action=editReview&revId='.$review['reviewId'].'">edit review</a></li>
                            
                            <li><a href="/acme/reviews/index.php?action=deleteReview&revId='.$review['reviewId'].'">delete review</a></li></ul></div>';  
                        }
                        $reviewDisplay .='<hr><br><hr><br>';
                            }
                    
                        }
                        if(isset($_SESSION['clientData']['clientId'])){
                            $clientData = $_SESSION['clientData']['clientId'];
                            $reviewDisplay .= '<div class="reviewLinks"><a href="/acme/reviews/index.php?action=addReview&clientId='
                            .$clientData.'&invId='.$review['invId'].'">add a review</a></div>';
                            }else{
                            $clientData = '';
                            }

                $reviewDisplay .= '</form></div>';

        return $reviewDisplay;
                }
            }
    
function getReviewTextbyReviewId($revId){
            $db = acmeConnect();
            $sql = 'SELECT reviewText FROM reviews WHERE reviewId = :reviewId';
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':reviewId', $revId, PDO::PARAM_INT);
            $stmt->execute();
            $clientName = $stmt->fetch();
            $stmt->closeCursor();
            return $clientName;
        }


function getReviewInfoByClientId($ClientId){
            $db = acmeConnect();
            $sql = 'SELECT reviewText, invId, reviewId FROM reviews WHERE clientId = :clientId';
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':clientId', $ClientId, PDO::PARAM_INT);
            $stmt->execute();
            $reviewInfo = $stmt->fetchAll();
            $stmt->closeCursor();
            return $reviewInfo;
        }

function getInvNameByInvId($invId){
            $db = acmeConnect();
            $sql = 'SELECT invName FROM inventory WHERE invId = :invId';
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':invId', $invId, PDO::PARAM_INT);
            $stmt->execute();
            $clientName = $stmt->fetch();
            $stmt->closeCursor();
            return $clientName;
}