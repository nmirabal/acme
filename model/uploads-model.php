<?php

/*
*El modelo es para interacciones de base de datos
*
*
*Este modelo es para subir imágenes de productos.
*/
// Add image information to the database table
function storeImages($imgPath, $invId, $imgName) {
    $db = acmeConnect();
    $sql = 'INSERT INTO images (invId, imgPath, imgName) VALUES (:invId, :imgPath, :imgName)';
    $stmt = $db->prepare($sql);
    // Store the full size image information
    $stmt->bindValue(':invId', $invId, PDO::PARAM_INT);
    $stmt->bindValue(':imgPath', $imgPath, PDO::PARAM_STR);
    $stmt->bindValue(':imgName', $imgName, PDO::PARAM_STR);
    $stmt->execute();
        
    // Make and store the thumbnail image information
    // Change name in path
    $imgPath = makeThumbnailName($imgPath);
    // Change name in file name
    $imgName = makeThumbnailName($imgName);
    $stmt->bindValue(':invId', $invId, PDO::PARAM_INT);
    $stmt->bindValue(':imgPath', $imgPath, PDO::PARAM_STR);
    $stmt->bindValue(':imgName', $imgName, PDO::PARAM_STR);
    $stmt->execute();
    
    $rowsChanged = $stmt->rowCount();
    $stmt->closeCursor();
    return $rowsChanged;
   }


   //La siguiente funcion tiene una coneccion con el controlador de upload images en el default case:
   // Get Image Information from images table
function getImages() {
    $db = acmeConnect();
    //Estamos utilizando un "JOIN" porque queremos obtener el nombre y la identificación de la tabla de "inventario" 
    //junto con la información de la imagen porque cada imagen está "vinculada" a un elemento del inventario.
    //El JOIN nos permitirá hacer eso en base al "invid" que se encuentra en ambas tablas.
    $sql = 'SELECT imgId, imgPath, imgName, imgDate, inventory.invId, invName FROM images JOIN inventory ON images.invId = inventory.invId';
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $imageArray = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $stmt->closeCursor();
    return $imageArray;
   }


// Delete image information from the images table
function deleteImage($id) {
    $db = acmeConnect();
    $sql = 'DELETE FROM images WHERE imgId = :imgId';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':imgId', $id, PDO::PARAM_INT);
    $stmt->execute();
    $result = $stmt->rowCount();
    $stmt->closeCursor();
    return $result;
   }


   // Check for an existing image-(checkExistingImage)->Se encuentra en el case 'upload':
function checkExistingImage($imgName){
    $db = acmeConnect();
    $sql = "SELECT imgName FROM images WHERE imgName = :name";
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':name', $imgName, PDO::PARAM_STR);
    $stmt->execute();
    $imageMatch = $stmt->fetch();
    $stmt->closeCursor();
    return $imageMatch;
   }


function getProductThumbnails($invId){
    $db = acmeConnect();
    $sql = 'SELECT imgPath, imgName FROM images WHERE invId = :invId AND imgPath LIKE "%-tn%"';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':invId', $invId, PDO::PARAM_INT);
    $stmt->execute();
    $imageMatch = $stmt->fetchAll();
    $stmt->closeCursor();
    return $imageMatch;
}
