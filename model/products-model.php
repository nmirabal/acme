<?php
//Products Model



// Insert New Product Y entre parentesis agregamos los ID de la lista que esta en la base de datos referente a el nuevo producto.

function newProd($categoryId, $invName, $invDescription, $invImage, $invThumbnail, $invPrice, $invStock, $invSize, $invWeight, $invLocation, $invVendor, $invStyle) {
    //Crea la coneccion entre el frontend y el backend
 $db = acmeConnect();
 $sql = 'INSERT INTO inventory (categoryId, invName, invDescription, invImage, invThumbnail, invPrice, invStock, invSize, invWeight, 
 invLocation, invVendor, invStyle) VALUES (:categoryId, :invName, :invDescription, :invImage, :invThumbnail, :invPrice, :invStock, :invSize, 
 :invWeight, :invLocation, :invVendor, :invStyle)';
 $stmt = $db->prepare($sql);
    //ponemos un blind value, que no se bien para que se utiliza, POR CADA ID
    //Should be PDO::PARAM_INT since field is an integer not a string. Esta hablando de categoryId
 $stmt->bindValue(':categoryId', $categoryId, PDO::PARAM_INT);
 $stmt->bindValue(':invName', $invName, PDO::PARAM_STR);
 $stmt->bindValue(':invDescription', $invDescription, PDO::PARAM_STR);
 $stmt->bindValue(':invImage', $invImage, PDO::PARAM_STR);
 $stmt->bindValue(':invThumbnail', $invThumbnail, PDO::PARAM_STR);
 $stmt->bindValue(':invPrice', $invPrice, PDO::PARAM_STR);
 $stmt->bindValue(':invStock', $invStock, PDO::PARAM_INT);
 $stmt->bindValue(':invSize', $invSize, PDO::PARAM_INT);
 $stmt->bindValue(':invWeight', $invWeight, PDO::PARAM_INT);
 $stmt->bindValue(':invLocation', $invLocation, PDO::PARAM_STR);
 $stmt->bindValue(':invVendor', $invVendor, PDO::PARAM_STR);
 $stmt->bindValue(':invStyle', $invStyle, PDO::PARAM_STR);
 $stmt->execute();
 $rowsChanged = $stmt->rowCount();
 $stmt->closeCursor();
 return $rowsChanged;
}

//La siguiente función obtendrá información básica del producto de la tabla de inventario para iniciar un proceso de actualización o eliminación.
function getProductBasics() {
   $db = acmeConnect();
   //La consulta (o query) solo obtiene el nombre y la identificación de cada artículo del inventario. Eso es todo lo que usamos para construir la tabla.
   $sql = 'SELECT invName, invId FROM inventory ORDER BY invName ASC';
   $stmt = $db->prepare($sql);
   $stmt->execute();
   /*
    Estamos utilizando el método fetchAll (PDO :: FETCH_ASSOC)
    para obtener una matriz asociativa para cada elemento. 
    El significado del nombre de campo de la tabla de la base de datos 
    se utiliza para identificar cada valor del producto.
   */
   $products = $stmt->fetchAll(PDO::FETCH_ASSOC);
   $stmt->closeCursor();
   return $products;
  }


  //en el siguiente code block, estamos seleccionando un solo producto en funcion del ID:
  // Get product information by invId
function getProductInfo($invId){
   $db = acmeConnect();
   $sql = 'SELECT * FROM inventory WHERE invId = :invId';
   $stmt = $db->prepare($sql);
   $stmt->bindValue(':invId', $invId, PDO::PARAM_INT);
   $stmt->execute();
   $prodInfo = $stmt->fetch(PDO::FETCH_ASSOC);
   $stmt->closeCursor();
   return $prodInfo;
  }




  //esta funcion actualizará un producto
   function updateProduct($categoryId, $invName, $invDescription, $invImage, $invThumbnail, $invPrice, $invStock, $invSize, $invWeight, $invLocation, $invVendor, $invStyle, $invId) {
   //Crea la coneccion entre el frontend y el backend
   $db = acmeConnect();
   $sql = 'UPDATE inventory SET categoryId = :categoryId, invName = :invName, invDescription = :invDescription, invImage = :invImage,
    invThumbnail = :invThumbnail, invPrice = :invPrice, invStock = :invStock, invSize = :invSize, invWeight = :invWeight, 
    invLocation = :invLocation, invVendor = :invVendor, invStyle = :invStyle WHERE invId = :invId';
   $stmt = $db->prepare($sql);
   //ponemos un blind value, que no se bien para que se utiliza, POR CADA ID
   //Should be PDO::PARAM_INT since field is an integer not a string. Esta hablando de categoryId
      $stmt->bindValue(':categoryId', $categoryId, PDO::PARAM_INT);
      $stmt->bindValue(':invName', $invName, PDO::PARAM_STR);
      $stmt->bindValue(':invDescription', $invDescription, PDO::PARAM_STR);
      $stmt->bindValue(':invImage', $invImage, PDO::PARAM_STR);
      $stmt->bindValue(':invThumbnail', $invThumbnail, PDO::PARAM_STR);
      $stmt->bindValue(':invPrice', $invPrice, PDO::PARAM_STR);
      $stmt->bindValue(':invStock', $invStock, PDO::PARAM_INT);
      $stmt->bindValue(':invSize', $invSize, PDO::PARAM_INT);
      $stmt->bindValue(':invWeight', $invWeight, PDO::PARAM_INT);
      $stmt->bindValue(':invLocation', $invLocation, PDO::PARAM_STR);
      $stmt->bindValue(':invVendor', $invVendor, PDO::PARAM_STR);
      $stmt->bindValue(':invStyle', $invStyle, PDO::PARAM_STR);
      $stmt->bindValue(':invId', $invId, PDO::PARAM_INT);
   $stmt->execute();
   $rowsChanged = $stmt->rowCount();
   $stmt->closeCursor();
   return $rowsChanged;
   }


//esta es la coneccion con la parte de DELETE PRODUCT
   function deleteProduct($invId) {
      $db = acmeConnect();
      $sql = 'DELETE FROM inventory WHERE invId = :invId';
      $stmt = $db->prepare($sql);
      $stmt->bindValue(':invId', $invId, PDO::PARAM_INT);
      $stmt->execute();
      $rowsChanged = $stmt->rowCount();
      $stmt->closeCursor();
      return $rowsChanged;
     }
   
     //Nueva función obtendrá una lista de productos basados ​​en la categoría.
     //getProductsByCategory: primero lo agregamos en the products controller dentro de un nuevo case 'category':
      function getProductsByCategory($categoryName){
      $db = acmeConnect();
      //La segunda parte, la subconsulta, es donde obtenemos el valor para que coincida con la categoríaId: IN (SELECCIONE categoryId FROM categories WHERE categoryName =: categoryName)
      //La palabra clave "IN" se refiere al valor que se devolverá de la subconsulta.
      /*La subconsulta solicita el ID de categoría basado en una coincidencia del nombre de categoría en la tabla de la base de datos
      *que coincidirá con el nombre de categoría (por ejemplo: nombre de categoría) que enviaremos a la consulta como un parámetro con nombre.
      */   
      $sql = 'SELECT * FROM inventory WHERE categoryId IN (SELECT categoryId FROM categories WHERE categoryName = :categoryName)';
      $stmt = $db->prepare($sql);
      $stmt->bindValue(':categoryName', $categoryName, PDO::PARAM_STR);
      $stmt->execute();
      $products = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $stmt->closeCursor();
      return $products;
     }


















     ?>