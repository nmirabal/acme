<?php
/*
* Reviews CONTROLLER
*
*/
// Create or access a Session
 session_start();
// Get the database connection file
 require_once '../library/connections.php';
 // Get the acme model for use as needed
 require_once '../model/acme-model.php';
//Get the accounts model
require_once '../model/products-model.php';
//Get the functions page
require_once '../library/functions.php';
//Get the function from uploads-model:
require_once '../model/uploads-model.php';
//Get the function from reviews-model:
require_once '../model/reviews-model.php';

// Get the array of categories
$categories = getCategories();
//var_dump($categories);
//exit; 

$action = filter_input(INPUT_POST, 'action');
 if ($action == NULL){
  $action = filter_input(INPUT_GET, 'action');
 }


// Build a navigation bar using the $categories array
 $navList = dynamicNavigation($categories); 

if(isset($_COOKIE['firstname'])){
    $cookieFirstname = filter_input(INPUT_COOKIE, 'firstname', FILTER_SANITIZE_STRING);
}


switch ($action) {

    case 'addReview':
                
                $invId = filter_input(INPUT_GET, 'invId',  FILTER_SANITIZE_NUMBER_INT);
                $clientId = filter_input(INPUT_GET, 'clientId',  FILTER_SANITIZE_NUMBER_INT);

                if (empty($invId) || empty($clientId)) {
                    $message = "oops, something went wrong, so lets try again";
                    include '../view/addReview.php';
                    exit;
                }
                include '../view/addReview.php';

                break;
               
            
        
    case "addedReview":
            $reviewText = filter_input(INPUT_POST, 'reviewText',  FILTER_SANITIZE_STRING);
            $invId = filter_input(INPUT_POST, 'invId',  FILTER_SANITIZE_NUMBER_INT);
            $clientId = filter_input(INPUT_POST, 'clientId',  FILTER_SANITIZE_NUMBER_INT);

            if (empty($clientId) || empty($invId) || empty($reviewText)) {
                $message = "oops, something went wrong, so lets try again";
                include '../view/addReview.php';
            }
            $rowsChanged = insertReview($clientId, $invId, $reviewText);  

            if($rowsChanged > 0 ){
                $message = 'the review was added';
                $_SESSION['message'] = $message;
                include '../view/admin.php';
            }else{
                $message = 'the review was added';
                $_SESSION['message'] = $message;
            include '../view/addReview.php';
            }

            break;

    case 'editReview':
            $revId = filter_input(INPUT_GET, 'revId', FILTER_SANITIZE_NUMBER_INT);
            $_SESSION['reviewId'] = $revId;
            if (empty($revId)) {
                $message = "oops";
                include '../view/editReview.php';
            }
            $reviewText = getReviewTextbyReviewId($revId);
            
                include '../view/editReview.php';
        break;

    case 'updateReview':

            $editReviewText = filter_input(INPUT_POST, 'editReviewText', FILTER_SANITIZE_STRING); 

            if (empty($editReviewText)) {
                $message = "please enter a review, and then click update, thanks";
                include '../view/editReview.php';
                exit;
            }
            $reviewId = $_SESSION['reviewId'];

            $updatedReview = updateReview($reviewId, $editReviewText);
            

            if(empty($updatedReview)){
                $message = "this review did not update...please try again or contact the Customer service department";
                $_SESSION['message']= $message;
                include '../view/editReview.php';
                exit;
            }else {
                $message = "the update was successful";
                $_SESSION['message']= $message;
                include '../view/admin.php';
                exit;
            }
            
               
        break;

    case 'deleteReview':
            $revId = filter_input(INPUT_GET, 'revId', FILTER_SANITIZE_NUMBER_INT);

            if (empty($revId)) {
                $message = "oops, something went wrong, please try again";
                include '../view/deleteReview.php';
            }
            $_SESSION['reviewId'] = $revId;
            $reviewText = getReviewTextbyReviewId($revId);
                    include '../view/deleteReview.php';
                break;

    case 'deletedReview':
            $revId = filter_input(INPUT_POST, 'reviewId', FILTER_SANITIZE_NUMBER_INT);

            $rowsDeleted = deleteReview($revId);
            if(!$rowsDeleted == 1){
                $message = "something went wrong";
                $_SESSION['message'] = $message; 
                include '../view/deleteReview.php';
                exit;
            } else {
                $message = "the delete was successful";
                $_SESSION['message'] = $message;
                include '../view/admin.php';
                exit;
            }

            
            break;

        
    default:
            
            //buildReviewDisplay($reviewList);
            

            include '../view/prod-detail.php';

        break;
                }