 <!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="Natasha Mirabal" content="BYU-I Online CIT336">
	<link rel="stylesheet" type="text/css" href="/acme/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="/acme/css/home.css">
    <link rel="stylesheet" type="text/css" href="/acme/css/medium.css">
    <link rel="stylesheet" type="text/css" href="/acme/css/large.css">
    <!--EL IF DENTRO DE TITLE SIRVE PARA QUE: Cuando se carga la página, el nombre del producto aparecerá en la pestaña de título del navegador.
     O, si la página se devuelve para la corrección de errores, el nombre del producto volverá a aparecer desde la variable del producto.-->
    <title>| Acme, Inc</title>
    <link rel="icon" href="/acme/images/site/coyote.png" type="image/png">
		<!-- STYLE INSIDE PHP CODE -->

    
<!-- BODY HERE -->
    
<body>
<?php
if (isset ($_SESSION ['loggedin'])){
$clientFirstname = $_SESSION ['clientData'] ['clientFirstname'];
}
?>
    <div id="containerall">
    <div class="header">
<header>
    <div class="containerA">
    <a href="/acme/index.php">
       <img src="/acme/images/site/logo.gif" width="200" height="100" alt="logo_acme" >
    </a>
    
<?php
    if (isset($_SESSION['loggedin'])) {
    //if logged in display welcome message, name, option to log out
        echo "<p><a href='/acme/accounts/index.php?action=loggedin'>Welcome $clientFirstname </a>
        <img id='file' src='/acme/images/site/account.gif' alt='The red folder'> <a href='/acme/accounts/index.php?action=logout'>Logout</a></p>";
    }
     else {
    //if not logged in, display the red folder and the My Account text
        echo "<img id='file' src='/acme/images/site/account.gif' alt='The red folder'>
        <a id='myAccount' href='/acme/accounts/index.php?action=login'>My Account</a>";
    }
?>    

    </div> 
</header>
</div>
