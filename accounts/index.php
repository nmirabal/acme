<?php
/*
* ACCOUNTS CONTROLLER
*
*/
$action = filter_input(INPUT_POST, 'action');
 if ($action == NULL){
  $action = filter_input(INPUT_GET, 'action');
 }
// Create or access a Session
 session_start();
// Get the database connection file
require_once '../library/connections.php';
//Get the functions page
require_once '../library/functions.php';
// Get the acme model for use as needed
require_once '../model/acme-model.php';
//Get the accounts model
require_once '../model/products-model.php';
//Get the function from uploads-model:
require_once '../model/uploads-model.php';
//Get the function from reviews-model:
require_once '../model/accounts-model.php';
//Get the function from reviews-model:
require_once '../model/reviews-model.php';

// Get the array of categories
$categories = getCategories();
//var_dump($categories);
//exit; 

// Build a navigation bar using the $categories array
 $navList = dynamicNavigation($categories);
 

  switch ($action){
    case 'login':
     include '../view/login.php';
     break;
    
        case 'process-login':
            $clientEmail = filter_input(INPUT_POST, 'clientEmail', FILTER_SANITIZE_EMAIL);
            //The checkEmail() function sanitizes the email string and then validates that it is a valid email. 
            //If it is, the valid email is returned, but if not "NULL" is returned.
            $clientEmail = checkEmail($clientEmail);
            $clientPassword = filter_input(INPUT_POST, 'clientPassword', FILTER_SANITIZE_STRING);
            //The checkPassword() function checks that the password meets the requirements
            // for our password and returns a "1" if it does or a "0" if it doesn't.
            $passwordCheck = checkPassword($clientPassword);
            
            // Run basic checks, return if errors
            if (empty($clientEmail) || empty($passwordCheck)) {
             $message = '<p class="notice">Please provide a valid email address and password.</p>';
             include '../view/login.php';
             exit;
            }
            //destroy cookie
            setcookie('firstName', '', strtotime('-1 year'), '/'); 
            // A valid password exists, proceed with the login process
            // Query the client data based on the email address
            $clientData = getClient($clientEmail);
            // Compare the password just submitted against
            // the hashed password for the matching client
            //--The password_verify function has the ability to hash the new password using the same 
            //information as when the original password was hashed, but using the new password--
            $hashCheck = password_verify($clientPassword, $clientData['clientPassword']);
            // If the hashes don't match create an error
            // and return to the login view
            if(!$hashCheck) {
              $message = '<p class="notice">Please check your password and try again.</p>';
              include '../view/login.php';
              exit;
            }
            // A valid user exists, log them in
            $_SESSION['loggedin'] = TRUE;
            // Remove the password from the array
            // the array_pop function removes the last
            // element from an array
            array_pop($clientData);

            // Store the array into the session
            $_SESSION['clientData'] = $clientData;
            // Send them to the admin view
            include '../view/admin.php';
            exit;
        break;

    case 'loggedin':
        include $_SERVER['DOCUMENT_ROOT'].'/acme/view/admin.php';
        exit;
        break;
   
    case 'logout':
       session_destroy();
       header('Location: /acme/index.php');
        exit;
       break;
        

    case 'register':
        include '../view/registration.php';
        break;
    
    case 'process-registration':
    // Filter and store the data- FILTER_SANITIZE_STRING: This "flag" removes any html elements and leaves only text-the data being filtered is to be treated as text and any HTML tags are to be removed - FILTER_SANITIZE_STRING.
    $clientFirstname = filter_input(INPUT_POST, 'clientFirstname', FILTER_SANITIZE_STRING);
    $clientLastname = filter_input(INPUT_POST, 'clientLastname', FILTER_SANITIZE_STRING);
    //the EMAIL input represents a special type of string you will add a different flag to it. Use the FILTER_SANITIZE_EMAIL 
    $clientEmail = filter_input(INPUT_POST, 'clientEmail', FILTER_SANITIZE_EMAIL);
    $clientPassword = filter_input(INPUT_POST, 'clientPassword', FILTER_SANITIZE_STRING);
//Because the code is executed from right to left, the original $clientEmail variable's value is passed to the function and the returned value is sent back and stored in the $clientEmail variable on the left.
    $clientEmail = checkEmail($clientEmail);
//As previously explained, the code is executed from right to left. The original $clientPassword variable's value is passed to the function and the returned value is sent back and stored in the $checkPassword variable on the left.
    //What is returned from the function is "1" if the password matches the format and a "0" (zero) if it doesn't.
    $checkPassword = checkPassword($clientPassword);
    

    //checking for an existing email address
    $existingEmail = checkExistingEmail($clientEmail);
    // Check for existing email address in the table
    if($existingEmail){
    $message = '<p class="notice">That email address already exists. Do you want to login instead?</p>';
    //en caso de que el email ya existe esta funcion include, nos redirecciona a la pagina de login, para loguearnos en lugar de crear una cuenta
    include '../view/login.php';
    exit;
    }


    //en la opcion de empty password cambiamos el valor de $clientPassword por $checkPassword
  //Why? By sending the original password variable to our custom function for comparison against our regular expression we are already checking that something is there. If it is empty or doesn't match a zero is returned. However, if it does match then a 1 is returned. By sending the $checkPassword variable into our list of "empty()" checks it will be empty if the password doesn't match our requirements for any reason, but it will NOT be empty if it does.
    if(empty($clientFirstname) || empty($clientLastname) || empty($clientEmail) || empty($checkPassword)){
// Check for missing data-EL USUARIO DEBE COMPLETAR TODOS LOS CAMPOS REQUERIDOS SI/IF NO LO HACE APARECERÁ UN MENSAJE DE QUE DEBE HACERLO
     $message = '<p>Please provide information for all empty form fields.</p>';
     include '../view/registration.php';
     exit; 
}
// Hash the checked password-Esto sirve para alterar una contaseña en la base de datos con muchos caracteres que no tienen sentido alguno para las personas algo como $2y$10$YmAIkuq.dKKNnbmjfnTJZOQFQccewPfZ/38F8a.kGlu eso seria un hash y se hace con el siguiente codigo:       
// Hash the checked password
$hashedPassword = password_hash($clientPassword, PASSWORD_DEFAULT);
        
// Send the data to the model
$regOutcome = regClient($clientFirstname, $clientLastname, $clientEmail, $hashedPassword);
            
 // Check and report the result
if($regOutcome === 1){
/*setcookie (1°parametro:string_name, 2°: string:$value, 3°:expire-o la vida de la cookie- 
    *en este caso será de un año, 4° is the path of the cookie  This parameter tells the server what folders within
    * the web site have access and can "see" the cookie. We will set the path to "/" which means the cookie will be visible to the entire web site.: "/" )
    *NOTE: normally you would use a calculation of multiplying seconds to equal a year, but using the strtotime() 
    *function and passing a value of + 1 year is much less writing and easier to understand
    */ 
    setcookie('firstName', $clientFirstname, strtotime('+1 year'), '/');
    
    $_SESSION['message'] = "Thanks for registering $clientFirstname. Please use your email and password to login.";
    //header () actúa como una redirección, que requiere que el servidor se vuelva a cargar y elimina todos los datos POST del formulario
    header('Location: /acme/accounts/?action=login');
    //include () actúa como un reenvío, simplemente incluye la vista y conserva todos los datos POST del formulario.
    //Al conservar los datos de la POST, puede causar la duplicación en su base de datos sin darse cuenta, lo que puede 
    //dar lugar a problemas de integridad de los datos.Esto puede no ser un problema para los datos del producto, pero es un problema
    // para las cuentas de clientes cuando una dirección de correo electrónico debe ser única.
    //include '../view/login.php';
    exit;
    } else {
    $message = "<p>Sorry $clientFirstname, but the registration failed. Please try again.</p>";
    include '../view/registration.php';
    exit;
    }    
    break;
        

   //esto pertenece a la parte de abajo que trata de actualizar toda la informacion del cliente
 case 'updatePage':
 include '../view/client-update.php';
 break;

    case 'updateClientInfo':
    $clientId = filter_input(INPUT_POST, 'clientId', FILTER_SANITIZE_NUMBER_INT);
    //var_dump($clientId);
    $newclientFirstname = filter_input(INPUT_POST, 'newclientFirstname', FILTER_SANITIZE_STRING);
    //var_dump($newclientFirstname);
    $newclientLastname = filter_input(INPUT_POST, 'newclientLastname', FILTER_SANITIZE_STRING);
    //var_dump($newclientLastname);
    $newclientEmail = filter_input(INPUT_POST, 'newclientEmail', FILTER_SANITIZE_EMAIL);

    $newclientEmail = checkEmail($newclientEmail);
    $existingEmail = checkExistingEmail($newclientEmail);
    //var_dump($existingEmail);

        // Check for match with current session email
    if($newclientEmail != $_SESSION['clientData']['clientEmail']){
        if ($existingEmail === 1) {
        $message = '<p>This email is already in use.</p>';
        include $_SERVER['DOCUMENT_ROOT'] . '/acme/view/client-update.php';
        exit;
        }
    }
    if (empty($newclientFirstname) || empty($newclientLastname) || empty($newclientEmail)){
        $message = '<p>All fields are required.</p>';
        include $_SERVER['DOCUMENT_ROOT'] . '/acme/view/client-update.php';
        exit;
    }

    $updateResult = updateClient($newclientFirstname, $newclientLastname, $newclientEmail, $clientId);
    //var_dump($updateResult);

    if ($updateResult){
        $message = "<p class='update-message'>Your account has been updated!</p>";
        $_SESSION['message'] = $message;
        $_SESSION['clientData']['clientFirstname'] = $newclientFirstname;
        $_SESSION['clientData']['clientLastname'] = $newclientLastname;
        $_SESSION['clientData']['clientEmail'] = $newclientEmail;
        include $_SERVER['DOCUMENT_ROOT'] . '/acme/view/admin.php';
        exit;
        } else {
        $message = "<p>Sorry, something went wrong. Please try again.</p>";
        include $_SERVER['DOCUMENT_ROOT'] . '/acme/view/client-update.php';
        exit;
        }
    exit;
break;

case 'updateClientPassword':
    $clientId = filter_input(INPUT_POST, 'clientId', FILTER_SANITIZE_NUMBER_INT);
    $clientPassword = filter_input(INPUT_POST, 'clientPassword', FILTER_SANITIZE_STRING);
    $checkPassword = checkPassword($clientPassword);
    if (empty($checkPassword)){
    $passwordMessage = '<p>Looks like you did not enter valid password.</p>';
    include $_SERVER['DOCUMENT_ROOT'] . '/acme/view/client-update.php';
    exit;
    }
    $hashedPassword = password_hash($clientPassword, PASSWORD_DEFAULT);
    $newPasswordOutcome = updatePassword($hashedPassword, $clientId);
    //var_dump($newPasswordOutcome);
    if($newPasswordOutcome === 1){ 
        $message = "<p class='update-message'>Password updated!</p>";
        include $_SERVER['DOCUMENT_ROOT'] . '/acme/view/admin.php';
        exit;
    } else {
        $message = "<p>Sorry, something went wrong. Please try again.</p>";
        include $_SERVER['DOCUMENT_ROOT'] . '/acme/view/admin.php';
        exit;
    }
break;



//CIERRE DEL switch:
  }
?>