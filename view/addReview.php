<!-- GOD IS GOOD -->
<?php 
    if(!$_SESSION['loggedin']){
        header('location: /acme/index.php');
    }
?><!DOCTYPE html>
<html lang="en">
<head>
    <title>Add Review</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/home.css">

</head>

<body>

<?php $ptitle='Account Information'; include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/nav.php'; ?>
    <?php
        if (isset($message)) {
        echo $message;}
    ?>
    <div class="main">
        <main>
        <h1>Add a review</h1>
        <br>
        <h3>Please, add the review and then click update to submit the new review</h3>
        
        
        <form action="index.php" id="addReviewForm" name="addReviewForm" method="post">
            
            <textarea name="reviewText" form="addReviewForm" id="addReviewText" maxlength="300" rows="7" columns="33" 
            required placeholder="Add your review right here."></textarea><br>
            <span id="pw_spec_notice"><strong>Once your review is ready, click "add review" to submit the changes</strong></span>
            <br>
            <br>
            <div class="buttons">
                <input type="submit" value="Add Review"/>
                <input type="hidden" name="invId" value="<?php echo $invId; ?>"/>
                <input type="hidden" name="clientId" value="<?php echo $clientId; ?>"/>
                <input type="hidden" name="action" value="addedReview"/>
            </div>
        </form>
        </main>
    </div>

</body>
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php'; ?>
