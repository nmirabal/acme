<?php $ptitle='login'; include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/nav.php'; ?>
<!--checks that a client is "loggedin" AND has a clientLevel is less than "2" to access the view.
 If not, redirect the client back to the acme controller to deliver the acme home view.-->
 <?php
if ($_SESSION['clientData']['clientLevel'] < 2) {
 header('location: /acme/');
 exit;
}
?>
<!doctype html>
<html lang="en">
<head>
    <!--EL IF DENTRO DE TITLE SIRVE PARA QUE: Cuando se carga la página, el nombre del producto aparecerá en la pestaña de título del navegador.
     O, si la página se devuelve para la corrección de errores, el nombre del producto volverá a aparecer desde la variable del producto.-->
    <title><?php if(isset($prodInfo['invName'])){ echo "Delete $prodInfo[invName]";} ?> | Acme, Inc.</title>
	<!-- STYLE INSIDE PHP CODE -->
</head>

<div class="main">
 <main>
<!-- usando este php dentro de un h1 estamos personalizando la pagina, en este caso para que aparezca en nombre del producto a modificar -->
 <h1><?php if(isset($prodInfo['invName'])){ echo "Delete $prodInfo[invName]";} ?></h1>
 <p>Confirm Product Deletion. The delete is permanent.</p>
 <!--mensaje en caso de que no se complete el formulario, este $message sale de product/index.php--> 
     <?php
if (isset($message)) {
 echo $message;
}
?>

<!-- El atributo action sirve para enviar la informacion donde nosotros queremos en este caso al archivo index.php de la carpeta products y se enviará cuando se haga click en el boton enviar -->
<form method="post" action="/acme/products/index.php">
    
    <fieldset>
       <legend>Delition of Product</legend><br>

<!--dentro de cada input agreremos un elseif para mostrar los datos de la matriz $prodInfo asi como de una variable
si estamos enviando datos para su reparacion y queremos que las entradas del formulario esten "pegajosas" o se que la info
quede cargada en cada campo   -->
       <div class="input">
        <label for="invName">Product Name: </label><br>
        <input type="text" readonly name="invName" id="invName" maxlength="50" <?php
         if(isset($prodInfo['invName'])) {echo "value='$prodInfo[invName]'"; }?>>
       </div>


       <div class="input">
        <label for="invDescription">Product Description:</label><br>
        <textarea 
         name="invDescription" readonly id="invDescription" maxlength="50" 
         placeholder="Enter description"<?php 
         if(isset($prodInfo['invDescription'])) {echo $prodInfo['invDescription']; } ?>>
        </textarea>
       </div>


        <div class="button">
        <input type="submit" name="submit" value="Delete Product" id="deleteProd">        
       </div>

       <!-- Add the action name - value pair to process the new product name -->
       <input type="hidden" name="action" value="deleteProd">
    <!--Ahora agregaremos una segunda entrada oculta para almacenar el valor de clave principal para el producto que se está actualizando 
        Esto nos permite enviar la clave principal para el producto sin que sea obvio para el cliente que mira el formulario. 
        Además, si la página se incluye para la corrección de errores, el valor de la clave principal
        del proceso de comprobación de errores) se devuelve y se vuelve a insertar en el campo oculto. -->
       <input type="hidden" name="invId" value="<?php
       if(isset($prodInfo['invId'])){ echo $prodInfo['invId'];} ?>">
      </fieldset>
</form>
     

</main>



<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php'; ?>
</div>