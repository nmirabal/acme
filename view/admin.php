<?php 
if(!$_SESSION['loggedin']){
header('location: /acme/index.php');
}
?><!DOCTYPE html>
<?php $ptitle='Account Information'; include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/nav.php'; ?>
<div class="main">
<main>
    <h1>Welcome <?php echo $_SESSION['clientData'] ['clientFirstname']; ?> </h1>
    <?php if (isset($message)) {echo $message;} ?>
    <p> You are logged in</p>

    <ul>
        <li>Name: <?php echo $_SESSION ['clientData'] ['clientFirstname']; ?></li>
        <li>Last Name: <?php echo $_SESSION ['clientData'] ['clientLastname']; ?></li>
        <li>Email:<?php echo $_SESSION ['clientData'] ['clientEmail']; ?></li>
    </ul>

    <a href="/acme/accounts/index.php?action=updatePage">Update Account Information</a>

    <?php
    if ($_SESSION ['clientData'] ['clientLevel'] > 1) {
        echo '<hr />
              <h2>Admin Tools</h2>
              <p>To add, edit and delete products, use the link below</p>
              <p><a href="/acme/products/index.php">Manage Products</a></p>';
    }
    ?>
 <div id="accountClientReviews">
                 <ul id="clientReviewsList">
                    <?php 
                    $reviewsInfo = getReviewInfoByClientId($_SESSION['clientData']['clientId']);
                    if(isset($reviewsInfo[0]['reviewId'])){echo '<h2>Below is a list of reviews that you have added</h2>';}
                    else{echo'<li>You have not made any reviews</li>';}
                    $reviewDisplay = '';

                    foreach($reviewsInfo as $review){
                        $prodName = getInvNameByInvId($review['invId']);
                        
                        $reviewDisplay .= '<li>Your review for the item:<br>'.$prodName['invName'].'</li>';
                        
                        $reviewDisplay .= '<li style="background:#eee;">'.$review['reviewText'].'</li>';
                        $reviewDisplay .= '<li><a href="/acme/reviews/index.php?action=editReview&revId='.$review['reviewId'].
                        '">edit review</a></li>';
                        $reviewDisplay .= '<li><a href="/acme/reviews/index.php?action=deleteReview&revId='.$review['reviewId'].
                        '">delete review</a></li>';
                        $reviewDisplay .= '<li id="reviewSpacer"></li>';
                        
                    }
                    echo $reviewDisplay;
                    ?>
                </ul>
               
            </div>

</main>

<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php'; ?>
</div>
