<!-- GOD IS GOOD -->
<?php 
    if(!$_SESSION['loggedin']){
        header('location: /acme/index.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Review</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--example media query link-->
    <link rel="stylesheet" type="text/css" href="../css/home.css">

</head>

<body>

<?php $ptitle='Account Information'; include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/nav.php'; ?>
    <?php
        if (isset($message)) {
        echo $message;}
        ?>
    
    <div class="main">
        <h1>Edit your review:</h1>
        <br>
        <p>please edit the review and then click update to submit it</p>
    
    <form action="index.php" id="reviewEditForm" name="reviewEditForm" method="post">
            
            <textarea name="editReviewText" form="reviewEditForm" id="editReviewText" maxlength="300" rows="7" columns="33" 
            required><?php if(isset($reviewText)){echo $reviewText['reviewText'];} ?></textarea><br>
            <span id="pw_spec_notice"><strong>Once your review is ready, click update to submit the changes</strong></span>
            <br>
            <br>
            <div class="buttons">
                <input type="submit" value="Update"/>
                <input type="hidden" name="action" value="updateReview"/>
            </div>
        </form>
    </div>

</body>
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php'; ?>
