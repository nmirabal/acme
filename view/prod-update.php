<?php
//Esto sirve para la lista desplegable de categorias cuando se agrega un nuevo producto en la base de datos
$catList = '<select name="categoryId" id="categoryId">';
$catList .= " <option>Select a Category</option>";
foreach ($categories as $category) {
  $catList .= "<option id='$category[categoryId]' value='$category[categoryId]'";
  if(isset($categoryId)){
    if($category['categoryId'] === $categoryId){
      $catList .= ' selected ';
    }
  } elseif(isset($prodInfo['categoryId'])){
         if($category['categoryId'] === $prodInfo['categoryId']){
            $catList .= ' selected ';
        }
  }
  $catList .= ">$category[categoryName]</option>";
}
$catList .='</select>';
?>
<!-- aca arriba temina la funcion para seleccionar la categoria de la lista desplegable-->
<?php $ptitle='login'; include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/nav.php'; ?>

<!--checks that a client is "loggedin" AND has a clientLevel is less than "2" to access the view.
 If not, redirect the client back to the acme controller to deliver the acme home view.-->
 <?php
if ($_SESSION['clientData']['clientLevel'] < 2) {
 header('location: /acme/');
 exit;
}
?>
<!doctype html>
<html lang="en">
<head>
	  <!--EL IF DENTRO DE TITLE SIRVE PARA QUE: Cuando se carga la página, el nombre del producto aparecerá en la pestaña de título del navegador.
     O, si la página se devuelve para la corrección de errores, el nombre del producto volverá a aparecer desde la variable del producto.-->
    <title><?php if(isset($prodInfo['invName'])){ echo "Modify $prodInfo[invName] ";} elseif(isset($invName)) { echo $invName; }?> | Acme, Inc</title>
</head>

<div class="main">
 <main>
<!-- usando este php dentro de un h1 estamos personalizando la pagina, en este caso para que aparezca en nombre del producto a modificar -->
 <h1><?php if(isset($prodInfo['invName'])){ echo "Modify $prodInfo[invName] ";} elseif(isset($invName)) { echo $invName; }?></h1>
     <h3>Modify the product below</h3>
     <p><strong>All fields are required</strong></p>
 <!--mensaje en caso de que no se complete el formulario, este $message sale de product/index.php--> 
     <?php
if (isset($message)) {
 echo $message;
}
?>

<!-- El atributo action sirve para enviar la informacion donde nosotros queremos en este caso al archivo index.php de la carpeta products y se enviará cuando se haga click en el boton enviar -->
<form method="post" action="/acme/products/index.php">
    
    <fieldset>
       <legend>Modify Product</legend><br>
              

       <div class="input">
        Category: 
        <?php echo $catList; ?>
       </div>
<!--dentro de cada input agreremos un elseif para mostrar los datos de la matriz $prodInfo asi como de una variable
si estamos enviando datos para su reparacion y queremos que las entradas del formulario esten "pegajosas" o se que la info
quede cargada en cada campo   -->
       <div class="input">
        <label for="invName">Product Name: </label>
        <input type="text" name="invName" id="invName" maxlength="50" <?php if (isset($invName)) {
         echo "value='$invName'";
        }
        elseif(isset($prodInfo['invName'])) {echo "value='$prodInfo[invName]'"; }  
        ?>required>
       </div>


       <div class="input">
        <label for="invDescription">Product Description:</label><br>
        <textarea 
         name="invDescription" id="invDescription" maxlength="50" 
         placeholder="Enter description" <?php if (isset($invDescription)) {
         echo $invDescription;
        }
        elseif(isset($prodInfo['invDescription'])) {echo "value='$prodInfo[invDescription]'"; }
        ?> required>
        </textarea>
       </div>


       <div class="input">
        <label for="invImage">Product Image: </label>
        <input type="text" name="invImage" id="invImage" 
         value="/acme/images/no-image.png" <?php if (isset($invImage)) {
         echo "value='$invImage'";
        } 
        elseif(isset($prodInfo['invImage'])) {echo "value='$prodInfo[invImage]'"; }
        ?> required>       
       </div>


       <div class="input">
        <label for="invThumbnail">Product Thumbnail:</label>
        <input type="text" name="invThumbnail" id="invThumbnail"
         value="/acme/images/products/no-image.png" <?php if (isset($invThumbnail)) {
         echo "value='$invThumbnail'";
        } 
        elseif(isset($prodInfo['invThumbnail'])) {echo "value='$prodInfo[invThumbnail]'"; }
        ?> required>
       </div>


       <div class="input">
        <label for="invPrice">Product Price: </label>	
        <input type="number" name="invPrice" id="invPrice" min="0.01" step="0.01" 
         placeholder="Enter amount" <?php if (isset($invPrice)) {
         echo "value='$invPrice'";
        } 
        elseif(isset($prodInfo['invPrice'])) {echo "value='$prodInfo[invPrice]'"; }
       ?> required>
       </div>


       <div class="input">
        <label for="invStock">In Stock:	</label>
        <input type="number" name="invStock" id="invStock" <?php if (isset($invStock)) {
         echo "value='$invStock'";
        } 
        elseif(isset($prodInfo['invStock'])) {echo "value='$prodInfo[invStock]'"; }
        ?> required>
       </div>


       <div class="input">
        <label for="invSize">Shipping Size: </label>
        <input type="number" name="invSize" id="invSize"  <?php if (isset($invSize)) {
         echo "value='$invSize'";
        }
        elseif(isset($prodInfo['invSize'])) {echo "value='$prodInfo[invSize]'"; } 
        ?> required>
       </div>


       <div class="input">
        <label for="invWeight">Weight (lbs):  </label>
        <input type="number" name="invWeight" id="invWeight" <?php if (isset($invWeight)) {
         echo "value='$invWeight'";
        } 
        elseif(isset($prodInfo['invWeight'])) {echo "value='$prodInfo[invWeight]'"; }
        ?> required>
       </div>


       <div class="input">
        <label for="invLocation">Location: </label>
        <input type="text" name="invLocation" id="invLocation" maxlength="35" <?php if (isset($invLocation)) {
         echo "value='$invLocation'";
        } 
        elseif(isset($prodInfo['invLocation'])) {echo "value='$prodInfo[invLocation]'"; }
        ?> required>
       </div>


       <div class="input">
        <label for="invVendor">Vendor Name: </label>
        <input type="text" name="invVendor" id="invVendor" <?php if (isset($invVendor) ? $invVendor :'') {
         echo "value='$invVendor'";
        } 
        elseif(isset($prodInfo['invVendor'])) {echo "value='$prodInfo[invVendor]'"; }
        ?> required>
       </div>


       <div class="input">
        <label for="invStyle">Primary Material: </label>
        <input type="text" name="invStyle" id="invStyle" <?php if (isset($invStyle)) {
         echo "value='$invStyle'";
        }
        elseif(isset($prodInfo['invStyle'])) {echo "value='$prodInfo[invStyle]'"; }
        ?> required>
       </div>

       <div class="button">
        <input type="submit" name="submit" value="Update Product" id="updateProd">        
       </div>

       <!-- Add the action name - value pair to process the new product name -->
       <input type="hidden" name="action" value="updateProd">
    <!--Ahora agregaremos una segunda entrada oculta para almacenar el valor de clave principal para el producto que se está actualizando 
        Esto nos permite enviar la clave principal para el producto sin que sea obvio para el cliente que mira el formulario. 
        Además, si la página se incluye para la corrección de errores, el valor de la clave principal
        del proceso de comprobación de errores) se devuelve y se vuelve a insertar en el campo oculto. -->
       <input type="hidden" name="invId" value="<?php if(isset($prodInfo['invId'])){
            echo $prodInfo['invId'];} elseif(isset($invId)){ echo $invId; } ?>">
      </fieldset>
</form>
     

</main>

<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php'; ?>
</div>