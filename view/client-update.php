<?php
if (!$_SESSION['loggedin']) {
 header('Location: /acme/accounts/');
}
?>

<?php $ptitle='Home'; include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/nav.php'; ?>
<!--checks that a client is "loggedin" AND has a clientLevel is less than "2" to access the view.
 If not, redirect the client back to the acme controller to deliver the acme home view.-->
 <?php
if ($_SESSION['clientData']['clientLevel'] < 1) {
 header('location: /acme/');
 exit;
}
?>
<div class="main">
<main>
   <h1><?php echo $_SESSION['clientData']['clientFirstname']; ?></h1>
   <p><a href="/acme/accounts/index.php?action=loggedin">&#8592; Back to account</a></p>
    
   
    
      <?php if (isset($message)) {echo $message;} ?>
      <form method="post" action="/acme/accounts/" >
       
       <fieldset>
        
        <legend>Update name &amp; email</legend>
        
        <div class="input">
        <label>First Name</label><br>
        <input <?php if(isset($clientFirstname)){echo "value='$clientFirstname'";} ?>
               id="newclientFirstname"
               name="newclientFirstname"
               placeholder="Enter your first name.."
               required
               type="text">
        </div>
        
        <div class="input">
        <label>Last Name</label><br>
        <input <?php if(isset($clientLastname)){echo "value='$clientLastname'";} ?>
               id="newclientLastname"
               name="newclientLastname"
               placeholder="Enter your last name.."
               required
               type="text">
        </div>
        
        <div class="input">
        <label > Your Email</label><br>
        <input <?php if(isset($clientEmail)){echo "value='$clientEmail'";} ?>
               id="newclientEmail"
               name="newclientEmail"
               placeholder="Enter your email address.."
               required
               type="email">
        </div>

        <div class="button">
         <input name="submit" type ="submit" value="Update my info" >
        </div>
        
        <input name="action" type="hidden" value="updateClientInfo" >
        <input name="clientId"
               type="hidden"
               value="<?php echo $_SESSION['clientData']['clientId'];?>">

        
        </fieldset> 
      </form>
    
    
    <div>
      
      <?php if (isset($passwordMessage)) {echo $passwordMessage;} ?>
      <form method="post" action="/acme/accounts/">
       <fieldset>
        
        <legend>Update password</legend>
        
        <div class="input">
        <label for="clientPassword">New Password *</label><br>
        <input id="clientPassword"
               name="clientPassword"
               pattern="(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
               placeholder="Enter a new password"
               required
               type="password">
        </div>

        <div class="button">
        <input name="submit" type="submit" value="Update my password" >
        </div>

        <input name="action" type="hidden" value="updateClientPassword" >
        <input name="clientId"
               type="hidden"
               value="<?php echo $_SESSION['clientData']['clientId'];?>">
       </fieldset>
       <p style="color: red;">Note: Submitting this form will change your password!</p>
      </form>
      <p>* Passwords must be at least 8 characters and contain at least 1 number, 1 capital letter and 1 special character.</p>
    </div>


  </main>
  
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php'; ?>
</div>