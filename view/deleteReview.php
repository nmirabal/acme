<!-- GOD IS GOOD -->
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Delete Review</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--example media query link-->
    <link rel="stylesheet" type="text/css" href="../css/home.css">

</head>

<body>

<?php $ptitle='Account Information'; include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/nav.php'; ?>
    <?php
        if (isset($message)) {
        echo $message;}
        ?>
    
    <div class="main">
        <h1>Delete your review:</h1>
        <br>
        <h3>deleting this review can not be undone</h3>
    
        <form action="index.php" id="reviewEditForm" name="reviewDeleteForm" method="post">
            
            <textarea name="reviewDeleteTextarea" form="reviewDeleteForm" id="deleteReviewText" maxlength="300" rows="7" columns="33" 
            readonly required><?php if(isset($reviewText)){echo $reviewText['reviewText'];} ?>
            </textarea><br>
            <span id="pw_spec_notice"><strong>Once your review is ready, click update to submit the changes</strong></span>
            <br>
            <br>
            <div class="buttons">
                <input type="submit" value="Delete"/>
                <input type="hidden" name="reviewId" value="<?php echo $revId?>"/>
                <input type="hidden" name="action" value="deletedReview"/>
            </div>
        </form>
    </div>

</body>
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php'; ?>
