<?php $ptitle='login'; include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/nav.php'; ?>


<div class="main">
 <main>

   <h1>Acme Registration</h1>
     <strong>All fields are required</strong>

<?php
if (isset($message)) {
 echo $message;
}
?>
<!-- Los codigos PHP en cada campo a completar ayudan a que la pagina web sea mas amigable, para que los usuarios NO tengan que completar todos los datos nuevamente si algo ocurre, NUNCA se pone este codigo PHP en el password porque SIEMPRE  se va a requerir que se complete el password-Do not do this for the password field! We always make the visitor re-type the password - Always! -->  
<form method="post" action="/acme/accounts/index.php">
        <fieldset>
            <label for="clientFirstname">First name:</label><br>
            <input type="text" name="clientFirstname" id="clientFirstname"
            <?php if(isset($clientFirstname)){
            echo "value='$clientFirstname'";
                }  
             ?> required><br> 
            
            <label for="clientLastname"> Last Name:</label><br>
            <input type="text" name="clientLastname" id="clientLastname"
            <?php if(isset($clientLastname)){
            echo "value='$clientLastname'";
                }  
             ?> required><br>
            
               <label for="clientEmail">Email Address:</label><br>
               <input type="email" name="clientEmail" id="clientEmail" placeholder="Enter a valid email address"
            <?php if(isset($clientEmail)){
            echo "value='$clientEmail'";
                }  
             ?> required><br>
            
               <label for="clientPassword">Password:</label><br>
            <span>Passwords must be at least 8 characters and contain at least 1 number, 1 capital letter and 1 special character</span><br>
             <!-- Pattern is a JavaScript regular expression  -->   
            <input type="password" name="clientPassword" id="clientPassword" required pattern="(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"><br>
           
           <input class="button" type="submit" value="register" id="register">
           
        <!-- Add the action name - value pair to process logging in -->
                <input type="hidden" name="action" value="process-registration">
       
       </fieldset>        
</form>
               
           
</main>

    
	<!-- Footer HERE -->
    


 <?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php'; ?>
 </div>
