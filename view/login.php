<?php $ptitle='login'; include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/nav.php'; ?>

<div class="main">
 
<main>

  <h1>Acme Login</h1>
    
    <?php
    if (isset($_SESSION['message'])) {
    echo $_SESSION['message'];
   }
?>

      <form action="/acme/accounts/index.php" method="post" >
          <fieldset>
              <label for="clientEmail">Email Address:</label><br>
               <input type="email" name="clientEmail" id="clientEmail" <?php if(isset($clientEmail)){
            echo "value='$clientEmail'";
                }  
             ?> required><br>
              
              <label for="clientPassword">Password:</label><br>
              <span>Passwords must be at least 8 characters and contain at least 1 number, 1 capital letter and 1 special character</span><br>
               <input type="password" name="clientPassword" id="clientPassword" required pattern="(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"><br>
              
              <input class="button" type="submit" value="login">
              
              <!-- Add the action name - value pair to process logging in -->
                <input type="hidden" name="action" value="process-login">
          </fieldset>
      </form>
  <h2>Not a member?</h2>

      <form method="post" action="http://localhost/acme/accounts/index.php?action=register">
          <input class="button" type="submit" value="create account">
      </form>
</main>


    
	<!-- Footer HERE -->
    
 <?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php'; ?>
 </div>
