<?php $ptitle='Home'; include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/nav.php'; ?>

<!-- MAIN HERE -->
  <div class="main">  
 <main>
      <h1>Welcome to ACME</h1>
   <div class="coyote">
     <div class="infoImg">
       <h2> Acme Rocket</h2>
        <p>Quick lighting fuse</p>
        <p>NHTSA approved seat belts </p>
         <p>Mobile launch stand included</p><a href="/acme/cart/"><img id="actionbtn" alt="Add to cart button" src="/acme/images/site/iwantit.gif"></a>
     </div>
   </div>
<!-- FEATURE RECIPES --> 
     
<div class="containerR">
 <div class="recipes">
    <h2>Featured Recipes</h2>
<table>
    <tr>
       <td width="222">
              <img  src="/acme/images/recipes/bbqsand.jpg" alt="bbq recipe" width="100" height="87" >
                <strong><a href="">Pulled Roadrunner BBQ</a></strong>
        </td>
        <td width="223">
             
              <img src="/acme/images/recipes/potpie.jpg" alt="potpie recipe" width="90" height="85" class="Ppieimg">
                 <strong><a href="">Roadrunner Pot Pie</a></strong>
          </td>
      </tr>
    <tr>
          <td>
               <img src="/acme/images/recipes/soup.jpg" alt="soup recipe" width="95" height="92" class="Soupmg">
                <strong><a href="">Roadrunner Soup</a></strong>
          </td>
          <td>
               <img src="/acme/images/recipes/taco.jpg" alt="tacos recipe" width="88" height="86" class="tacosimg">
                <strong><a href="">Roadrunner Tacos</a></strong>
          </td>
                  
        </tr>
  </table>
</div>
<div class="acmerockets">
    <h2>Acme Rocket Reviews</h2>
      <ul>
        <li>"I don't know how I ever caught roadrunners before this." (4/5)</li><br>
        <li>"That thing was fast!" (4/5)</li><br>
        <li>"Talk about fast delivery." (5/5)</li><br>
        <li>"I didn't even have to pull the meat apart." (4.5/5)</li><br>
        <li>"I'm on my thirtieth one. I love these things!" (5/5)</li>
      </ul>
</div>
</div>
</main>

<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php'; ?>
</div>
