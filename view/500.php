<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="Natasha Mirabal" content="BYU-I Online CIT336">
    <link rel="stylesheet" type="text/css" href="../css/template.css">
	<link rel="stylesheet" type="text/css" href="../acme/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="../acme/css/medium.css">
    <link rel="stylesheet" type="text/css" href="../acme/css/large.css">
	<title>acme</title>
		<!-- STYLE INSIDE PHP CODE -->
<style type="text/css">
    body{
    text-align:center;
    margin:0 auto;
    }
    #contenedor{
    margin: 0 auto;
    text-align:left;
    width:70%;
    }
</style>
</head>
    
<!-- BODY HERE -->
    
<body>
<div id="contenedor">
 <div class="header">
     <header>
		<img class="logoacme" src="../images/site/logo.gif" alt="logo_acme">
		<img class="carpetaroja" src="../images/site/account.gif" alt="my_account">
        <h3 class="PalabraMyacc">My account</h3>
         
     <!-- NAVIGATION BAR HERE -->
    
  <div class="menu">
       <nav>
          <ul>
            <li><a href="home">Home</a></li>
            <li><a href="cannon">Cannon</a></li>
            <li><a href="explosive">Explosive</a></li>
            <li><a href="misc">Misc</a></li>
	        <li><a href="rocket">Rocket</a></li>
            <li><a href="trap">Trap</a></li>
          </ul>
       </nav>
    </div>

   </header>
</div>

<div class="main">
 <main>
<h1>Server Error</h1>
     
</main>
</div>
    
	<!-- Footer HERE -->
    

<div class="footer">
 <footer>
<p>&copy; ACME, All rights reserved.<br>
All images used are believed to be in "Fair Use". Please notify the author if any are not and they will be removed.</p>
 </footer>
</div>
</div>
</body>
</html>
