<?php $ptitle='Home'; include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/nav.php'; ?>
<!--para verificar si hay un mensaje configurado en $ _SESSION-->
<?php
if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
   }
?>

<div class="main">
<main>
<h1>Add New Product Image</h1>
<?php
 if (isset($message)) {
  echo $message;
 } ?>
<!--La "action" dirige el formulario para enviar los datos al controlador que se encuentra en la carpeta de UPLOADS-->
<!-- enctype="multipart/form-data": No se codifican los caracteres. Este valor es obligatorio cuando está utilizando
 formularios que tienen un control de carga de archivos.-->
<form action="/acme/uploads/" method="post" enctype="multipart/form-data">
 <label for="invItem">Product</label><br>
 <!--$ prodSelect indica dónde se mostrará la lista de selección de los productos en la base de datos.-->
 <?php echo $prodSelect; ?><br><br>
 <label>Upload Image:</label><br>
 <!--type="file". Esto permite que el navegador abra un nuevo cuadro de diálogo para que pueda seleccionar un archivo para cargar.-->
 <input type="file" name="file1"> <br>
 <input type="submit" class="regbtn" value="Upload"><br>
 <input type="hidden" name="action" value="upload">
</form>
<hr>

<h2>Existing Images</h2>
<p class="notice">If deleting an image, delete the thumbnail too and vice versa.</p>
<?php
 if (isset($imageDisplay)) {
  echo $imageDisplay;
 } ?>

</main>


<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php'; ?>
</div>
<!--PHP y el código para cancelar el mensaje de la sesión, ya que hemos terminado de usarlo:-->
<?php unset($_SESSION['message']); ?>