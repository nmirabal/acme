<?php
//Esto sirve para la lista desplegable de categorias cuando se agrega un nuevo producto en la base de datos
$catList = '<select name="categoryId" id="categoryId">';
$catList .= " <option>Select a Category</option>";
foreach ($categories as $category) {
  $catList .= "<option id='$category[categoryId]' value='$category[categoryId]'";
  if(isset($categoryId)){
    if($category['categoryId'] === $categoryId){
      $catList .= ' selected ';
    }
  } elseif(isset($prodInfo['categoryId'])){
         if($category['categoryId'] === $prodInfo['categoryId']){
            $catList .= ' selected ';
        }
  }
  $catList .= ">$category[categoryName]</option>";
}
$catList .='</select>';
?>
<!-- aca arriba temina la funcion para seleccionar la categoria-->


<?php $ptitle='login'; include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/nav.php'; ?>

<!--checks that a client is "loggedin" AND has a clientLevel is less than "2" to access the view.
 If not, redirect the client back to the acme controller to deliver the acme home view.-->
 <?php
if ($_SESSION['clientData']['clientLevel'] < 2) {
 header('location: /acme/');
 exit;
}
?>
<div class="main">
 <main>

   <h1>Add Product</h1>
     <h2>Add a new product below</h2><br>
     <strong>All fields are required</strong>
     <p><a href="/acme/accounts/index.php?action=loggedin">&#8592; Back to Product Management</a></p>
 <!--mensaje en caso de que no se complete el formulario, este $message sale de product/index.php--> 
     <?php
if (isset($message)) {
 echo $message;
}
?>

<!-- El atributo action sirve para enviar la informacion donde nosotros queremos
 en este caso al archivo index.php de la carpeta products y se enviará cuando se haga click en el boton enviar -->
<form method="post" action="/acme/products/index.php">
    
    <fieldset>
       <legend>Add Product</legend>

       <div class="input">
        Category: 
        <?php echo $catList; ?>
       </div>

       <div class="input">
        <label for="invName">Product Name: </label>
        <input type="text" name="invName" id="invName" maxlength="50" <?php if (isset($invName)) {
         echo "value='$invName'";
        }  
        ?>required>
       </div>

       <div class="input">
        <label for="invDescription">Product Description:</label><br>
        <textarea 
         name="invDescription" id="invDescription" maxlength="50" 
         placeholder="Enter description" <?php if (isset($invDescription)) {
         echo $invDescription;
        } ?> required>
        </textarea>
       </div>

       <div class="input">
        <label for="invImage">Product Image: </label>
        <input type="text" name="invImage" id="invImage" 
         value="/acme/images/no-image.png" <?php if (isset($invImage)) {
         echo "value='$invImage'";
        } 
        ?> required>       
       </div>

       <div class="input">
        <label for="invThumbnail">Product Thumbnail:</label>
        <input type="text" name="invThumbnail" id="invThumbnail"
         value="/acme/images/products/no-image.png" <?php if (isset($invThumbnail)) {
         echo "value='$invThumbnail'";
        } 
        ?> required>
       </div>

       <div class="input">
        <label for="invPrice">Product Price: </label>	
        <input type="number" name="invPrice" id="invPrice" min="0.01" step="0.05" 
         placeholder="Enter amount" <?php if (isset($invPrice)) {
         echo "value='$invPrice'";
        } 
        ?> required>
       </div>

       <div class="input">
        <label for="invStock">In Stock:	</label>
        <input type="number" name="invStock" id="invStock" <?php if (isset($invStock)) {
         echo "value='$invStock'";
        } 
        ?> required>
       </div>

       <div class="input">
        <label for="invSize">Shipping Size: </label>
        <input type="number" name="invSize" id="invSize"  <?php if (isset($invSize)) {
         echo "value='$invSize'";
        } 
        ?> required>
       </div>

       <div class="input">
        <label for="invWeight">Weight (lbs):  </label>
        <input type="number" name="invWeight" id="invWeight" <?php if (isset($invWeight)) {
         echo "value='$invWeight'";
        } 
        ?> required>
       </div>

       <div class="input">
        <label for="invLocation">Location: </label>
        <input type="text" name="invLocation" id="invLocation" maxlength="35" <?php if (isset($invLocation)) {
         echo "value='$invLocation'";
        } 
        ?> required>
       </div>

       <div class="input">
        <label for="invVendor">Vendor Name: </label>
        <input type="text" name="invVendor" id="invVendor" <?php if (isset($invVendor) ? $invVendor :'') {
         echo "value='$invVendor'";
        } 
        ?> required>
       </div>

       <div class="input">
        <label for="invStyle">Primary Material: </label>
        <input type="text" name="invStyle" id="invStyle" <?php if (isset($invStyle)) {
         echo "value='$invStyle'";
        }
        ?> required>
       </div>

       <div class="button">
        <input type="submit" name="submit" value="Add Product" id="newProduct">        
       </div>

       <!-- Add the action name - value pair to process the new product name -->
       <input type="hidden" name="action" value="newProd">
      </fieldset>
</form>
     

</main>



<?php include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php'; ?>
</div>